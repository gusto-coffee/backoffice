import React from 'react';
import Navbar from '../components/Navbar';
export default function HomePage() {
  return (
    <div id='home-page'>
      <Navbar activeItem='home' />

      <h1>Home</h1>
    </div>
  );
}
