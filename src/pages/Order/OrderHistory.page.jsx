import React, { useState, useEffect } from 'react';
import Navbar from '../../components/Navbar';
import { Redirect } from 'react-router-dom';
import DataTable from 'react-data-table-component';
import { Header, Select, Container } from 'semantic-ui-react';
import * as API from '../../utils/API';
import { dateConverter } from '../../utils/DateConverter';
import isElectron from 'is-electron';

const dialog = window.electron.dialog;
const sortOptions = [
  {
    key: 't',
    value: 'today',
    text: "Aujourd'hui",
  },
  {
    key: 'w',
    value: 'week',
    text: 'Semaine',
  },
  {
    key: 'm',
    value: 'month',
    text: 'Mois',
  },
  {
    key: 'y',
    value: 'year',
    text: 'Année',
  },
];
export default function OrderHistoryPage() {
  const columns = [
    {
      name: 'id_order',
      selector: 'id_order',
      sortable: false,
    },
    {
      name: 'date livraison',
      selector: 'date',
      sortable: true,
    },
  ];
  const [state, setState] = useState([]);
  const [sort, setSort] = useState('today');
  const [redirect, setRedirect] = useState(false);
  const [id, setId] = useState(''); // id de la commande pour la redirection

  useEffect(() => {
    setState([]); // vider le tableau pour le rafraichissement
    API.getOrderHistory(sort)
      .then(({ data, status }) => {
        if (status === 200) {
          data.forEach((item, index) => {
            setState(prev => [
              ...prev,
              {
                _id: item._id,
                id_order: item.id_order,
                date: dateConverter(item.date.updated),
                index,
              },
            ]);
          });
        }
      })
      .catch(err => {
        console.log(err.response);
        if (isElectron()) {
          dialog.showErrorBox(
            'Erreur serveur',
            err.response.data.error.message,
          );
        } else alert(err.response.data.error.message);
      });
  }, [sort]);
  if (redirect) return <Redirect to={`/order/${id}`}></Redirect>;
  else
    return (
      <div id='order-history-page'>
        <Navbar activeItem='history' />
        <Container fluid>
          <Header as='h2'>Nombre de commandes livrées : {state.length}</Header>
          <Select
            options={sortOptions}
            value={sort}
            onChange={(e, { value }) => setSort(value)}
          />
          <DataTable
            columns={columns}
            data={state}
            dense
            responsive
            striped
            highlightOnHover
            pagination
            pointerOnHover
            onRowClicked={e => {
              setId(e._id);
              setRedirect(true);
            }}
            paginationRowsPerPageOptions={[10, 25, 50, 100]}
            language={{
              url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json',
            }}
          />
        </Container>
      </div>
    );
}
