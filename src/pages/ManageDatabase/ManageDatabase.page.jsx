import React from 'react';
import { Link } from 'react-router-dom';
import { Card, Container, Image } from 'semantic-ui-react';
import Navbar from '../../components/Navbar';
/**
 * Page des commandes en préparation + livrées
 */
export default function ManageDatabasePage() {
  return (
    <div id='manage-database-page'>
      <Navbar activeItem='db' />

      <Container id='cards-menu'>
        <Card.Group stackable centered itemsPerRow={6}>
          <Card link color='blue' title='Voir tous les produits'>
            <Link to='/manage-database/product'>
              <Image src='logo.png' />
              <Card.Content textAlign='center'>
                <Card.Header as='h3'>Gestion des produits</Card.Header>
              </Card.Content>
            </Link>
          </Card>

          <Card link color='brown' title='Voir toutes les catégories'>
            <Link to='/manage-database/category'>
              <Image src='https://media.gettyimages.com/vectors/cafe-icon-shape-designs-donut-vector-id1036418524' />
              <Card.Content textAlign='center'>
                <Card.Header as='h3'>Gestion des catégories</Card.Header>
              </Card.Content>
            </Link>
          </Card>

          <Card link color='yellow' title='Voir toutes les options'>
            <Link to='/manage-database/option'>
              <Image src='https://img.freepik.com/vecteurs-libre/pile-crepes-frites-bananes-sauce-au-chocolat-noix-delicieux-petit-dejeuner-illustration-dessin-anime_87693-54.jpg?size=338&ext=jpg' />
              <Card.Content textAlign='center'>
                <Card.Header as='h3'>
                  Gestion des options de produits
                </Card.Header>
              </Card.Content>
            </Link>
          </Card>

          <Card link color='blue' title='Voir tous les employés'>
            <Link to='/manage-database/user'>
              <Image src='https://optrium.fr/wp-content/uploads/2016/10/USERs.png' />
              <Card.Content textAlign='center'>
                <Card.Header as='h3'>Gestion des employés</Card.Header>
              </Card.Content>
            </Link>
          </Card>

          <Card link color='green' title='Voir tous les espaces de travail'>
            <Link to='/manage-database/coworking'>
              <Image src='https://img.freepik.com/vecteurs-libre/plan-bureau-cabine-coworking-employes-bureau-employes-leurs-lieux-travail_33099-239.jpg?size=338&ext=jpg' />
              <Card.Content textAlign='center'>
                <Card.Header as='h3'>
                  Gestion des catégories d&apos;emplacement
                </Card.Header>
              </Card.Content>
            </Link>
          </Card>

          <Card link color='purple' title='Voir tous les emplacements'>
            <Link to='/manage-database/location'>
              <Image src='https://img.freepik.com/vecteurs-libre/hommes-affaires-travaillant-ensemble_23-2148455576.jpg?size=338&ext=jpg' />
              <Card.Content textAlign='center'>
                <Card.Header as='h3'>Gestion des emplacements</Card.Header>
              </Card.Content>
            </Link>
          </Card>
        </Card.Group>
      </Container>
    </div>
  );
}
