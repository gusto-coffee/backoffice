import React from 'react';
import { Link } from 'react-router-dom';
import { Icon, Label, Menu } from 'semantic-ui-react';
import * as STORAGE from '../utils/Storage';
export default function Navbar({ activeItem }) {
  const state = {
    employee: STORAGE.get('employee'),
    role: STORAGE.get('role'),
    avatar:
      'https://fnadepape.org/wp-content/uploads/2018/07/avatar-1577909_960_720.png',
  };

  const { role, employee, avatar } = state;
  return (
    <div id='navbar'>
      <Menu icon tabular>
        <Link to='/order'>
          <Menu.Item as={'span'} name='order' active={activeItem === 'order'}>
            <Icon size='big' name='coffee' title='Afficher les commandes' />
            <p className={activeItem && 'active'}>Gestion des commandes</p>
          </Menu.Item>
        </Link>

        {/* si l'employé n'a pas les droits admin, l'affichage de cet onglet est masqué */}
        {isAdmin(role)}

        <Link to='/logout' id='logout'>
          <Menu.Item
            as={'span'}
            position='right'
            name='sign out'
            active={activeItem === 'sign out'}>
            <Icon
              size='big'
              name='sign out'
              color='red'
              title='Se déconnecter'
            />
            <p>Déconnexion</p>
          </Menu.Item>
        </Link>
      </Menu>

      {/* Info du profil connecté sous la barre de navigation à droite */}
      <div id='employee'>
        <Label as='a' color='blue' image>
          <img alt='avatar' src={avatar} />
          {employee}
          <Label.Detail>Rôle : {role}</Label.Detail>
        </Label>
      </div>
    </div>
  );

  /**
   * Vérifie si le rôle est bien admin
   * @param {String} role
   */
  function isAdmin(role) {
    if (role === 'admin')
      return (
        <>
          <Link to='/manage-database'>
            <Menu.Item as={'span'} name='db' active={activeItem === 'db'}>
              <Icon
                size='big'
                name='database'
                title='Voir tous les articles en base de données'
              />
              <p className={activeItem && 'active'}>
                Gestion des produits / employés
              </p>
            </Menu.Item>
          </Link>
          <Link to='/order-history'>
            <Menu.Item
              as={'span'}
              name='history'
              active={activeItem === 'history'}>
              <Icon
                size='big'
                name='history'
                title="Voir l'historique des commandes livrées"
              />
              <p className={activeItem && 'active'}>Historique</p>
            </Menu.Item>
          </Link>
        </>
      );
  }
}
