import React, { useState, useEffect } from 'react';
import { List, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import * as API from '../../utils/API';
import { dateConverter } from '../../utils/DateConverter';
import isElectron from 'is-electron';
import { socket as io } from '../../utils/SocketIO';

const dialog = window.electron.dialog;

export default function OrderPrepared() {
  const [orders, setOrders] = useState([]);
  const [socket, setSocket] = useState(null);
  useEffect(() => {
    setSocket(io);
    // demande au chargement de la page la récupération des commandes préparées
    API.getOrdersPrepared()
      .then(({ data, status }) => {
        if (status === 200) {
          setOrders(data);
        }
      })
      .catch(err => {
        console.error(err.response);
        if (isElectron()) {
          dialog.showErrorBox(
            'Erreur lors de la récupération des commandes préparées',
            err.response.data.error,
          );
        } else alert(err.response.data.error);
      });
  }, []);
  useEffect(() => {
    if (!socket) return;

    // réception des commandes préparées
    socket.on('orders-prepared', data => {
      setOrders(prev => [...prev, data]);
    });
  }, [socket]);

  // s'il n'y a pas de commandes à livrer on affiche un message au lieu de laisser le champ vide
  if (orders.length === 0) return 'Pas de commandes à livrer.';
  else
    return (
      <List id='order-prepared' divided relaxed>
        {orders.map((order, index) =>
          // affichage que si la commande est préparée
          order.status === 2 ? (
            <List.Item key={'orderdelivered' + index.toString()}>
              <div className='order-prepared-items'>
                <Icon
                  name='check circle outline'
                  onClick={updateStatus.bind(this, order._id)}
                  color='green'
                  size='big'
                  title='Confimer la livraison de la commande'
                />
                <List.Content>
                  <Link
                    title={'Voir commande ' + order.id_order}
                    role='listitem'
                    className='item'
                    to={'/order/' + order._id}>
                    <List.Header>{order.id_order}</List.Header>
                  </Link>

                  <List.Description className='date'>
                    Préparée le {dateConverter(order.date.updated)}
                  </List.Description>
                </List.Content>
              </div>
            </List.Item>
          ) : null,
        )}
      </List>
    );

  // faire passer la commande de non livrée à livrée quand elle a été préparée
  function updateStatus(_id, e) {
    e.preventDefault();
    let data = {
      _id,
      status: 3,
    };
    socket.emit('update-order-status', data, response => {
      if (response === 201) {
        setOrders(orders.filter(item => item._id !== _id));
      }
    });
  }
}
