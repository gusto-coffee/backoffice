import React, { useState } from 'react';
import {
  Button,
  Header,
  Form,
  Input,
  Message,
  Select,
  Image,
} from 'semantic-ui-react';
import * as API from '../../../utils/API';
import { Redirect } from 'react-router-dom';

const rolesOptions = [
  { key: 'e', text: 'Employé', value: 'employee' },
  { key: 'm', text: "Chef d'équipe", value: 'manager' },
  { key: 'a', text: 'Admin', value: 'admin' },
  { key: 'c', text: 'Client', value: 'client' },
];

/**
 * Formulaire d'ajout d'un nouvel employé en BDD
 */
export default function NewEmployee({ type = 'user' }) {
  const [state, setState] = useState({});
  const [redirect, setRedirect] = useState(false);

  // messages
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const [errorContent, setErrorContent] = useState('');

  if (redirect) return <Redirect to={`/manage-database/${type}`} />;
  else
    return (
      <div id='new-user'>
        <Header
          textAlign='center'
          as='h3'
          content="Ajout d'un nouvel utilisateur/employé"
          image={
            <Image
              src='https://cdn.pixabay.com/photo/2012/04/26/19/43/profile-42914_1280.png'
              size='massive'
              circular
            />
          }
        />
        <Form onSubmit={handleSubmit} success={success} error={error}>
          <Form.Group widths='equal'>
            {/* PRENOM */}
            <Form.Field required>
              <label>Prénom</label>
              <Input onChange={handleChange} name='firstname' required />
            </Form.Field>
            {/* NOM */}
            <Form.Field required>
              <label>Nom</label>
              <Input onChange={handleChange} name='lastname' required />
            </Form.Field>
          </Form.Group>

          <Form.Group widths='equal'>
            {/* COURRIEL */}
            <Form.Field required>
              <label>Courriel</label>
              <Input
                type='email'
                onChange={handleChange}
                name='email'
                required
              />
            </Form.Field>
            {/* MOT DE PASSE */}
            <Form.Field required>
              <label>Mot de passe</label>
              <Input
                type='password'
                onChange={handleChange}
                name='password'
                required
              />
            </Form.Field>
          </Form.Group>

          <Form.Group>
            {/* RÔLE */}
            <Form.Field
              required
              width='6'
              key='role'
              control={Select}
              label='Rôle'
              name='role'
              options={rolesOptions}
              placeholder={rolesOptions[0].text}
              onChange={handleChange}
            />
          </Form.Group>
          <Message
            success
            header='Succès'
            content={
              state.role === 'client'
                ? 'Utilisateur ajouté !'
                : 'Employé ajouté !'
            }
          />
          <Message
            error
            header="Erreur lors de l'envoi du formulaire !"
            content={errorContent}
          />
          <Form.Field>
            <Button
              primary
              content='Ajouter'
              type='submit'
              title='Ajouter un utilisateur, employé'
            />
            <Button
              color='grey'
              content='Annuler'
              title="Annuler l'opération en cours"
              type='button'
              onClick={() => {
                setRedirect(true);
              }}
            />
          </Form.Field>
        </Form>
      </div>
    );

  /**
   * Bouton Modifier dynamiquement les modèles ayant peu de champs comme option, category
   */
  function handleSubmit(e) {
    // mise à 0 si jamais il y a déjà eu un message
    setError(false);
    setSuccess(false);
    //
    e.preventDefault();
    API.signup(state)
      .then(({ status }) => {
        if (status === 201) {
          setSuccess(true); // affichage du message de succès
          setTimeout(() => {
            setRedirect(true); //redirection automatique
          }, 2500);
        }
      })
      .catch(err => {
        console.log(err.response);
        setErrorContent(JSON.stringify(err.response.data.error, null, '\t')); // affichage du contenu du message d'erreur
        setError(true); //  affichage du message d'erreur
      });
  }

  function handleChange(e, { name, value, type }) {
    setState(prevState => ({
      ...prevState,
      [name]: type === 'number' ? parseFloat(value) : value,
    }));
  }
}
