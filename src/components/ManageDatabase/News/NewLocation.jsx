import React, { useState, useEffect } from 'react';
import {
  Button,
  Header,
  Form,
  Input,
  Message,
  Select,
} from 'semantic-ui-react';
import * as API from '../../../utils/API';
import { Redirect } from 'react-router-dom';

/**
 * Formulaire de modification d'une catégorie en BDD
 */
export default function NewLocation({ type }) {
  const [state, setState] = useState({});
  const [redirect, setRedirect] = useState(false);
  // catégories des emplacements
  const [categories, setCategories] = useState([]);

  // messages
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const [errorContent, setErrorContent] = useState('');

  useEffect(() => {
    API.getLocationCategories()
      .then(({ data, status }) => {
        if (status === 200) {
          data.forEach(item => {
            setCategories(prev => [
              ...prev,
              {
                key: item._id.toString(),
                text: item.name,
                value: item._id,
              },
            ]);
          });
        }
      })
      .catch(err => {
        console.log(err);
        alert(err);
      });
  }, [type]);
  if (redirect) return <Redirect to={`/manage-database/${type}`} />;
  else
    return (
      <div id='new-location'>
        <Header
          textAlign='center'
          as='h3'
          content="Ajout d'un nouvel emplacement"
        />
        <Form onSubmit={handleSubmit} success={success} error={error}>
          <Form.Group widths='equal'>
            {/* NOM DE L'EMPLACEMENT */}
            <Form.Field required>
              <label>{"Nom de l'emplacement"}</label>
              <Input
                onChange={handleChange}
                name='name'
                placeholder='Moka'
                required
              />
            </Form.Field>

            {/* CATEGORIE DE L'EMPLACEMENT */}
            <Form.Field required>
              <label>{"Catégorie de l'emplacement"}</label>
              <Select
                name='location_category_id'
                onChange={handleChange}
                placeholder='Choisir une catégories'
                fluid
                required
                selection
                search
                options={categories}
              />
            </Form.Field>
          </Form.Group>

          <Message
            success
            header='Succès'
            content='Nouvel emplacement ajouté.'
          />
          <Message
            error
            header="Erreur lors de l'envoi du formulaire !"
            content={errorContent}
          />
          <Form.Field>
            <Button
              primary
              content='Ajouter'
              title="Ajouter une nouvelle catégorie d'emplacement"
              type='submit'
            />
            <Button
              color='grey'
              content='Annuler'
              title="Annuler l'opération en cours"
              type='button'
              onClick={() => {
                setRedirect(true);
              }}
            />
          </Form.Field>
        </Form>
      </div>
    );

  /**
   * Bouton Modifier dynamiquement les modèles ayant peu de champs comme option, category
   */
  function handleSubmit() {
    // mise à 0 si jamais il y a déjà eu un message
    setError(false);
    setSuccess(false);
    //
    API.newLocation(state)
      .then(({ status }) => {
        if (status === 201) {
          setSuccess(true); // affichage du message de succès
          setTimeout(() => {
            setRedirect(true); //redirection automatique
          }, 2500);
        }
      })
      .catch(err => {
        console.log(err.response);
        setErrorContent(JSON.stringify(err.response.data.error, null, '\t')); // affichage du contenu du message d'erreur
        setError(true); //  affichage du message d'erreur
      });
  }

  function handleChange(e, { name, value }) {
    setState(prevState => ({
      ...prevState,
      [name]: value,
    }));
  }
}
