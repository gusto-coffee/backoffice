import React, { useState } from 'react';
import {
  Button,
  Header,
  Form,
  Input,
  Message,
  Dropdown,
} from 'semantic-ui-react';
import * as API from '../../../utils/API';
import { Redirect } from 'react-router-dom';

const priceOptions = [
  { key: 'e', value: 'euro', text: '€' },
  { key: 'd', value: 'dollar', text: '$' },
  { key: 'l', value: 'livre', text: '£' },
];

/**
 * Formulaire de modification d'une catégorie en BDD
 */
export default function NewLocationCategory({ type }) {
  const [state, setState] = useState({});
  const [redirect, setRedirect] = useState(false);

  // messages
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const [errorContent, setErrorContent] = useState('');

  if (redirect) return <Redirect to={`/manage-database/${type}`} />;
  else
    return (
      <div id='new-location-category'>
        <Header
          textAlign='center'
          as='h3'
          content="Ajout d'une nouvelle catégorie d'emplacement"
        />
        <Form onSubmit={handleSubmit} success={success} error={error}>
          <Form.Group widths='equal'>
            {/* NOM DE LA CATEGORIE D'EMPLACEMENT */}
            <Form.Field required>
              <label>{"Nom de la catégorie d'emplacement"}</label>
              <Input
                required
                onChange={handleChange}
                name='name'
                placeholder='Grands salons privés'
              />
            </Form.Field>
            {/* EQUIPEMENTS DISPONIBLES */}
            <Form.Field>
              <label>Equipements disponibles</label>
              <Input
                onChange={handleChange}
                name='stuff'
                placeholder='Connexion Wifi / Paperboard / Écran'
              />
            </Form.Field>
          </Form.Group>

          <Form.Group widths='equal'>
            {/* PRIX DE L'EMPLACEMENT */}
            <Form.Field required>
              <label>Prix</label>
              {/* MONNAIE */}
              <Input
                required
                label={
                  <Dropdown
                    onChange={handleChange}
                    name='currency'
                    options={priceOptions}
                    value={priceOptions[0].value}
                  />
                }
                onChange={handleChange}
                labelPosition='right'
                step='0.01'
                type='number'
                name='price'
              />
            </Form.Field>
            {/* NOMBRE DE PLACES POUR CETTE CATEGORIE D'EMPLACEMENT */}
            <Form.Field required>
              <label>Nombre de places</label>
              <Input
                required
                type='number'
                onChange={handleChange}
                name='place_number'
              />
            </Form.Field>
            {/* POINTS DE FIDELITE */}
            <Form.Field>
              <label>Points de fidélité</label>
              <Input
                type='number'
                onChange={handleChange}
                name='place_number'
              />
            </Form.Field>
            {/* OFFRE DE FIDELITE */}
            <Form.Field>
              <label>Offre de fidélité</label>
              <Input type='number' onChange={handleChange} name='offer' />
            </Form.Field>
          </Form.Group>

          <Message
            success
            header='Succès'
            content="Nouvelle catégorie d'emplacement ajoutée."
          />
          <Message
            error
            header="Erreur lors de l'envoi du formulaire !"
            content={errorContent}
          />
          <Form.Field>
            <Button
              primary
              content='Ajouter'
              title="Ajouter une nouvelle catégorie d'emplacement"
              type='submit'
            />
            <Button
              color='grey'
              content='Annuler'
              title="Annuler l'opération en cours"
              type='button'
              onClick={() => {
                setRedirect(true);
              }}
            />
          </Form.Field>
        </Form>
      </div>
    );

  /**
   * Bouton Modifier dynamiquement les modèles ayant peu de champs comme option, category
   */
  function handleSubmit() {
    // mise à 0 si jamais il y a déjà eu un message
    setError(false);
    setSuccess(false);
    //
    API.newLocationCategory(state)
      .then(({ status }) => {
        if (status === 201) {
          setSuccess(true); // affichage du message de succès
          setTimeout(() => {
            setRedirect(true); //redirection automatique
          }, 2500);
        }
      })
      .catch(err => {
        console.log(err.response);
        setErrorContent(JSON.stringify(err.response.data.error, null, '\t')); // affichage du contenu du message d'erreur
        setError(true); //  affichage du message d'erreur
      });
  }

  function handleChange(e, { name, value }) {
    setState(prevState => ({ ...prevState, [name]: value }));
  }
}
