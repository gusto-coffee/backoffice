import React, { useState } from 'react';
import { Button, Header, Form, Input, Message } from 'semantic-ui-react';
import * as API from '../../../utils/API';
import { Redirect } from 'react-router-dom';

/**
 * Formulaire d'ajout d'une nouvelle catégorie en BDD
 */
export default function NewCategory({ type }) {
  const [state, setState] = useState({});
  const [redirect, setRedirect] = useState(false);

  // messages
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const [errorContent, setErrorContent] = useState('');

  if (redirect) return <Redirect to={`/manage-database/${type}`} />;
  else
    return (
      <div id='new-category'>
        <Header
          textAlign='center'
          as='h3'
          size='large'
          content={"Ajout d'une nouvelle catégorie"}
        />
        <Form onSubmit={handleSubmit} error={error} success={success}>
          {/* NOM DE LA CATEGORIE */}
          <Form.Field width='6' required>
            <label>Nom de la catégorie</label>
            <Input onChange={handleChange} name='name' required />
          </Form.Field>
          <Message
            success
            header='Succès'
            content='Nouvelle catégorie ajoutée !'
          />
          <Message
            error
            header="Erreur lors de l'envoi du formulaire !"
            content={errorContent}
          />

          <Button
            primary
            content='Ajouter'
            title='Ajouter la catégorie'
            type='submit'
          />
          <Button
            color='grey'
            content='Annuler'
            title="Annuler l'opération en cours"
            type='button'
            onClick={() => {
              setRedirect(true);
            }}
          />
        </Form>
      </div>
    );

  /**
   * Bouton ajouter une catégorie
   */
  function handleSubmit(e) {
    // mise à 0 si jamais il y a déjà eu un message
    setError(false);
    setSuccess(false);
    //
    e.preventDefault();
    API.newCategory(state)
      .then(({ status }) => {
        if (status === 201) {
          setSuccess(true); // affichage du message de succès
          setTimeout(() => {
            setRedirect(true); //redirection automatique
          }, 2500);
        }
      })
      .catch(err => {
        console.log(err.response);
        setErrorContent(JSON.stringify(err.response.data.error, null, '\t')); // affichage du contenu du message d'erreur
        setError(true); //  affichage du message d'erreur
      });
  }

  /**
   * Chnager la valeur du nom de la catégorie
   */
  function handleChange(e, { name, value }) {
    setState(prevState => ({ ...prevState, [name]: value }));
  }
}
