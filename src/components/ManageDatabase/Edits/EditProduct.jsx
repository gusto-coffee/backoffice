import isElectron from 'is-electron';
import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import {
  Button,
  Dropdown,
  Form,
  Header,
  Input,
  Loader,
  Message,
  TextArea,
} from 'semantic-ui-react';
import * as API from '../../../utils/API';

const priceOptions = [
  { key: 'e', value: 'euro', text: '€' },
  { key: 'd', value: 'dollar', text: '$' },
  { key: 'l', value: 'livre', text: '£' },
];

const dialog = window.electron.dialog;

/**
 * Formulaire de modification d'un produit en BDD
 */
export default function EditProduct({ id, type }) {
  // variable bloquante en attendant du chargement de toutes les données
  const [dataLoaded, setDataLoaded] = useState(false);

  // catégories et options disponibles en BDD
  const [categories, setCategories] = useState([]);
  const [options, setOptions] = useState([]);

  const [state, setState] = useState({});
  const [oldCategories, setOldCategories] = useState([]); // catégories du produit en bdd
  const [oldOptions, setOldOptions] = useState([]); // options du produit en bdd

  const [redirect, setRedirect] = useState(false);
  // messages
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const [errorContent, setErrorContent] = useState('');

  useEffect(() => {
    // récupération du produit
    API.getProduct(id)
      .then(({ data, status }) => {
        if (status === 200) {
          setState(data);

          // ajout de la catégorie du produit
          data.category_id.forEach(item => {
            setOldCategories(prev => [...prev, item._id]);
          });

          // ajout de l'id des options
          data.option_id.forEach(item => {
            setOldOptions(prev => [...prev, item._id]);
          });
          // récupérer toutes les catégories
          return API.getCategories();
        }
      })
      .then(({ data, status }) => {
        if (status === 200) {
          // ajout de toutes les catégories disponibles en BDD
          data.forEach(item => {
            setCategories(prev => [
              ...prev,
              {
                key: item._id.toString(),
                text: item.name,
                value: item._id,
              },
            ]);
          });
          // récupérer toutes les options
          return API.getOptions();
        }
      })
      .then(({ data, status }) => {
        if (status === 200) {
          // ajout de toutes les options disponibles en BDD
          data.forEach(item => {
            setOptions(prev => [
              ...prev,
              {
                key: item._id.toString(),
                text: item.name,
                value: item._id,
              },
            ]);
          });

          // quand toutes les données sont reçues
          setDataLoaded(true);
        }
      })
      .catch(err => {
        setDataLoaded(true);
        console.log(err.response);
        if (isElectron()) {
          dialog.showErrorBox('Erreur serveur', err.response.data.error);
        } else alert(err.response.data.error);
      });
  }, [id]);

  if (redirect) return <Redirect to={`/manage-database/${type}`} />;
  else if (!dataLoaded)
    return <Loader active indeterminate content='Chargement' />;
  else
    return (
      <div id='edit-product'>
        <Header
          icon='coffee'
          content={'Modification du produit : ' + state.name}
        />
        <Form onSubmit={handleSubmit} error={error} success={success}>
          <Form.Group widths='equal'>
            {/* NOM DU PRODUIT */}
            <Form.Field required>
              <label>Nom du produit</label>
              <Input
                onChange={handleChange}
                value={state.name}
                name='name'
                required
              />
            </Form.Field>

            {/* PRIX DU PRODUIT */}
            <Form.Field required>
              <label>Prix du produit</label>
              {/* MONNAIE */}
              <Input
                required
                label={
                  <Dropdown
                    onChange={handleChange}
                    name='currency'
                    value={state.currency}
                    options={priceOptions}
                  />
                }
                onChange={handleChange}
                value={state.price}
                labelPosition='right'
                step='0.01'
                type='number'
                name='price'
              />
            </Form.Field>
          </Form.Group>
          <Form.Group widths='equal'>
            {/* DESCRIPTION DU PRODUIT */}
            <Form.Field required>
              <label>Description</label>
              <TextArea
                maxLength='250'
                required
                rows={3}
                value={state.description}
                onChange={handleChange}
                placeholder='Café issu de grains ...'
                name='description'
              />
              <div className='characters-max'>
                <span>250 caractères maximum</span>
              </div>
            </Form.Field>

            <Form.Field>
              <Form.Group widths='equal'>
                {/* OFFRE DU PRODUIT */}
                <Form.Field>
                  <label>Offre</label>
                  <Input
                    onChange={handleChange}
                    value={state.offer}
                    name='offer'
                  />
                </Form.Field>

                {/* POINT DU PRODUIT */}
                <Form.Field>
                  <label>Points</label>
                  <Input
                    onChange={handleChange}
                    value={state.points}
                    name='points'
                  />
                </Form.Field>
              </Form.Group>
            </Form.Field>
          </Form.Group>
          <Form.Group widths='equal'>
            {/* CATEGORIE DU PRODUIT */}
            <Form.Field required>
              <label>Catégories</label>
              <Dropdown
                required
                name='category_id'
                fluid
                multiple
                search
                selection
                onChange={handleChange}
                value={oldCategories}
                options={categories}
              />
            </Form.Field>

            {/* OPTIONS DU PRODUIT */}
            <Form.Field>
              <label>Choix des options pour le produit</label>
              <Dropdown
                name='option_id'
                fluid
                multiple
                search
                selection
                onChange={handleChange}
                value={oldOptions}
                options={options}
              />
            </Form.Field>
          </Form.Group>
          <Message success header='Succès' content='Modification réussi !' />
          <Message
            error
            header="Erreur lors de l'envoi du formulaire !"
            content={errorContent}
          />
          <Form.Field>
            <Button
              primary
              content='Modifier'
              type='submit'
              title='Modifier le produit'
            />
            <Button
              color='red'
              content='Supprimer'
              title='Supprimer le produit'
              type='button'
              onClick={handleClickDelete}
            />
          </Form.Field>
        </Form>
      </div>
    );

  /**
   * Bouton valider la modification
   */
  function handleSubmit() {
    // mise à 0 si jamais il y a déjà eu un message
    setError(false);
    setSuccess(false);
    //
    let productCopy = state; // création d'une copie
    productCopy.category_id = oldCategories; // mise à jour des catégories
    productCopy.option_id = oldOptions; // mise à jour des options

    setState(productCopy); // mise à jour du produit pour l'envoyer

    API.manageDatabase(id, state, 'PATCH', type)
      .then(({ status }) => {
        if (status === 201) {
          setSuccess(true); // affichage du message de succès
          setTimeout(() => {
            setRedirect(true); //redirection automatique
          }, 2500);
        }
      })
      .catch(err => {
        console.log(err.response);
        setErrorContent(JSON.stringify(err.response.data.error, null, '\t')); // affichage du contenu du message d'erreur
        setError(true); //  affichage du message d'erreur
      });
  }

  function handleChange(e, { name, value, type }) {
    // modification des options
    if (name === 'option_id') {
      setOldOptions(value);
    }
    // modification de la catégorie
    else if (name === 'category_id') {
      setOldCategories(value);
    } else {
      setState(prevState => ({
        ...prevState,
        [name]: type === 'number' ? parseFloat(value) : value,
      }));
    }
  }

  /**
   * Bouton pour supprimer
   */
  function handleClickDelete() {
    if (isElectron())
      dialog
        .showMessageBox(null, {
          buttons: ['Oui', 'Non'],
          message: 'Confirmez-vous la suppression de ' + state.name + ' ?',
        })
        .then(({ response }) => {
          if (response === 0)
            API.manageDatabase(state._id, state, 'DELETE', type)
              .then(({ status }) => {
                if (status === 200) {
                  dialog.showMessageBox(null, {
                    message: 'Suppression réussi !',
                  });
                  setRedirect(true); //redirection automatique
                }
              })
              .catch(err => {
                console.log(err.response);
                if (isElectron()) {
                  dialog.showErrorBox(
                    'Erreur serveur',
                    err.response.data.error,
                  );
                } else alert(err.response.data.error);
              });
          else return;
        });
    else {
      const r = window.confirm(
        'Confirmez-vous la suppression de ' + state.name + ' ?',
      );
      if (r)
        API.manageDatabase(state._id, state, 'DELETE', type)
          .then(({ status }) => {
            if (status === 200) {
              alert('Suppression réussi !');
              setRedirect(true); //redirection automatique
            }
          })
          .catch(err => {
            console.log(err.response);
            alert(err.response.data.error);
          });
    }
  }
}
