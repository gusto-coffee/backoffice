import axios, { Method } from 'axios';
import * as STORAGE from './Storage';
import { url_api as __URL } from '../info.json';

/**
 * GET
 */

export function checkRole(): Promise<any> {
  return axios.request({
    url: __URL + '/admin/check-role',
    method: 'GET',
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function getProducts(): Promise<any> {
  return axios.request({
    url: __URL + '/product',
    method: 'GET',
  });
}

export function getProduct(id: string): Promise<any> {
  return axios.request({
    url: `${__URL}/product/${id}`,
    method: 'GET',
  });
}

export function getCategories(): Promise<any> {
  return axios.request({
    url: __URL + '/category',
    method: 'GET',
  });
}
export function getCategory(id: string): Promise<any> {
  return axios.request({
    url: `${__URL}/category/${id}`,
    method: 'GET',
  });
}

export function getOptions(): Promise<any> {
  return axios.request({
    url: __URL + '/option',
    method: 'GET',
  });
}
export function getOption(id: string): Promise<any> {
  return axios.request({
    url: `${__URL}/option/${id}`,
    method: 'GET',
  });
}

export function getEmployees(): Promise<any> {
  return axios.request({
    url: __URL + '/admin/users',
    method: 'GET',
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}
export function getEmployee(id: string): Promise<any> {
  return axios.request({
    url: `${__URL}/admin/user/${id}`,
    method: 'GET',
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function getLocationCategories(): Promise<any> {
  return axios.request({
    url: __URL + '/coworking',
    method: 'GET',
  });
}
export function getLocationCategory(id: string): Promise<any> {
  return axios.request({
    url: `${__URL}/coworking/${id}`,
    method: 'GET',
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function getLocations(): Promise<any> {
  return axios.request({
    url: __URL + '/coworking/location',
    method: 'GET',
  });
}
export function getLocation(id: string): Promise<any> {
  return axios.request({
    url: `${__URL}/coworking/location/${id}`,
    method: 'GET',
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function getOrdersPaid(): Promise<any> {
  return axios.request({
    url: __URL + '/admin/order/paid',
    method: 'GET',
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function getOrdersPrepared(): Promise<any> {
  return axios.request({
    url: __URL + '/admin/order/prepared',
    method: 'GET',
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function getOrderHistory(sort?: string): Promise<any> {
  return axios.request({
    url: __URL + '/admin/order/history',
    method: 'GET',
    params: {
      sort,
    },
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function getOrder(id_order: string): Promise<any> {
  return axios.request({
    url: __URL + '/admin/order/' + id_order,
    method: 'GET',
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

/**
 * POST
 */

/**
 *
 * @param {String} email
 * @param {String} password
 */
export function login(email: string, password: string): Promise<any> {
  return axios.request({
    url: __URL + '/auth/login', // la connexion se fait sur la même route que les utilisateurs pour récupérer un token
    method: 'POST',
    data: {
      email,
      password,
    },
  });
}

/**
 * Enregistrer un employé
 * @params {Object} data
 */
export function signup(data: Object): Promise<any> {
  return axios.request({
    url: __URL + '/admin/signup',
    method: 'POST',
    data,
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function newProduct(data: Object): Promise<any> {
  return axios.request({
    url: __URL + '/admin/product/new',
    method: 'POST',
    data,
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function newCategory(data: Object): Promise<any> {
  return axios.request({
    url: __URL + '/admin/category/new',
    method: 'POST',
    data,
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function newOption(data: Object): Promise<any> {
  return axios.request({
    url: __URL + '/admin/option/new',
    method: 'POST',
    data,
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function newLocationCategory(data: Object): Promise<any> {
  return axios.request({
    url: __URL + '/admin/coworking/new',
    method: 'POST',
    data,
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function newLocation(data: Object): Promise<any> {
  return axios.request({
    url: __URL + '/admin/coworking/location/new',
    method: 'POST',
    data,
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

/**
 * PATCH
 */

export function changePassword(id: string, data: Object): Promise<any> {
  return axios.request({
    url: `${__URL}/admin/user/${id}/edit-password`,
    method: 'PATCH',
    data,
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

export function updateOrderStatus(
  id_order: string,
  data: Object,
): Promise<any> {
  return axios.request({
    url: `${__URL}/admin/order/${id_order}`,
    method: 'PATCH',
    data,
    headers: {
      authorization: 'Bearer ' + STORAGE.get('token'),
    },
  });
}

/**
 * DYNAMIQUE
 */
/**
 * Modifier ou supprimer les éléments en BDD dynamiquement
 * id : id du produit
 * data : données à modifier
 * method : PATCH | DELETE
 * uriParam : category | product | option | coworking | location ...
 */
export function manageDatabase(
  id: string,
  data: Object,
  method: Method,
  uriParam: string,
): Promise<any> {
  // modifier/supprimer les emplacements qui se trouve dans la route /coworking/location
  if (uriParam === 'location')
    return axios.request({
      url: `${__URL}/admin/coworking/${uriParam}/${id}`,
      method,
      data,
      headers: {
        authorization: 'Bearer ' + STORAGE.get('token'),
      },
    });
  else
    return axios.request({
      url: `${__URL}/admin/${uriParam}/${id}`,
      method,
      data,
      headers: {
        authorization: 'Bearer ' + STORAGE.get('token'),
      },
    });
}
